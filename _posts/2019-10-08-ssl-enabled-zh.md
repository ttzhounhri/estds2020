---
layout: post
title: "SSL加密已经启用"
lang: zh
date: 2019-10-08
abstract: "我们已将整个网站部署了SSL加密，以更好地保护您的安全。"
fimage: "/assets/img/web-security.jpg"
fimage_credit: "Pixabay 免授权"
raction: "了解更多"
ref: ssl-enabled
---

我们认为您的安全最为重要。

从现在开始，我们的整个网站都将使用[SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security)加密。SSL加密可以在您使用我们的网站时，起到额外的保护作用。

{% include figure.html url="/assets/img/how-ssl-works-cn-dadian-2018.jpg" caption="SSL加密的运作方式 (Dadian, 2019)" %}

感谢您，祝您愉快！

## 参考资料
1. Dadian, D. (2019). SSL - what it means, how it works and where it is used. [online] *Powersolution.com*. Available [here](https://www.powersolution.com/ssl-what-it-means-how-it-works-whereused/) [Accessed 8 Oct. 2019].
