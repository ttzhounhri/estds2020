---
layout: post
title: "We are making the 1st Announcement"
lang: en
date: 2020-03-13
abstract: "Forum postponed to November and abstract submission open"
fimage: "/assets/img/xuanwu-lake-nanjing-zdc.jpg"
fimage_credit: "Dechao Zhang, CC BY-NC-SA 4.0"
ref: announcement-1
---
Sorry for keeping everyone waiting. Here is our [<i class="fas fa-file-pdf fa-fw mr-1"></i>1st Announcement](https://cdn.jsdelivr.net/gh/estds/estds2020/assets/doc/estds2020-announcement-1-en.pdf).

Due to the COVID-19 epidemic, we regret to announce that our Forum will be postponed to **16 to 19 November 2020**. If this change would cause any inconvenience, we are deeply sorry. Yet we do wish the epidemic could be contained and eliminated as soon as possible around the globe, and wish everyone who has been impacted a quick recovery.

Meanwhile, our abstract submission system is now ready and will be open until **15 July 2020**. You are most welcome to [submit your abstracts](/take-part).

---
## Background
{% include about-background.html %}

## Theme & topics

### Theme

- Water security and sustainability

### Topics
{% include about-topics.html %}

## Contacts
{% include contacts.html %}

## Download

You may [click here](https://cdn.jsdelivr.net/gh/estds/estds2020/assets/doc/estds2020-announcement-1-en.pdf) to download a copy of our 1st announcement.